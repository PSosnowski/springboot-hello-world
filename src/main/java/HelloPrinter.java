import lombok.extern.java.Log;
import org.springframework.stereotype.Component;

@Component
@Log
public class HelloPrinter {

    public static void sayHello() {
        log.info("Hello world!");
    }
}
